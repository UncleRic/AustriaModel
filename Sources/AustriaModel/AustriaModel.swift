
import Combine
import Foundation

public struct AustriaDataList: Codable {
    let updated: Int
    let provinces: [AustriaProvince]
    let districts: [AustriaDistrict]
    let percentageBySex: PercentageBySex
    let casesByAge, deathsByAge: [String: Int]
}

public struct AustriaDistrict: Codable {
    let district: String
    let cases, population: Int
}

public struct PercentageBySex: Codable {
    let cases, deaths: DeathCases
}

public struct DeathCases: Codable {
    let female, male: Int
}

public struct AustriaProvince: Codable {
    let province: String
    let cases, recovered, deaths: Int
}

// ----------------------------------------------------------
// Revision:

public struct RevisedAustriaDataList {
    public let provinces: [RevisedAustriaProvince]
    public let districts: [RevisedAustriaDistrict]
    public let percentageBySex: PercentageBySex
    public let casesByAge, deathsByAge: [String: Int]
}

public struct RevisedAustriaProvince: Identifiable {
    public let id: UUID
    public let province: String
    public let cases, recovered, deaths: String
}

public struct RevisedAustriaDistrict: Identifiable {
    public let id: UUID
    public let district: String
    public let cases, population: String
}

enum MyError: Error {
    case failed
    case inValidURL
    case httpError(Int)
    case missingData
}

@available(iOS 15.0.0, *)
public final class AustriaModel: ObservableObject {
    public static let shared = AustriaModel()
    @Published public var revisedAustriaDataList: RevisedAustriaDataList?

    public static var text = "Hello, World!"

    public init() {}

    // ==============================================================================================

    public func getAsyncData() async -> RevisedAustriaDataList? {
        print("*** Async Method ****")
        let result = try? await fetchDataWithAsyncURLSession()
        return result
    }

    // ----------------------------------------------------------------------------------------------
    @available(iOS 15.0.0, *)

    private func fetchDataWithAsyncURLSession() async throws -> RevisedAustriaDataList {
        guard let url = URL(string: "https://disease.sh/v3/covid-19/gov/Austria") else {
            throw MyError.inValidURL
        }

        // Use the async variant of URLSession to fetch data
        let (data, _) = try await URLSession.shared.data(from: url)

        // Parse the JSON data
        let austriaData = try JSONDecoder().decode(AustriaDataList.self, from: data)

        return add_UUID(origData: austriaData)
    }

    // ----------------------------------------------------------------------------------------------
    private func add_UUID(origData: AustriaDataList) -> RevisedAustriaDataList {
        var districts: [RevisedAustriaDistrict] = []
        var provinces: [RevisedAustriaProvince] = []

        // Districts:
        for item in origData.districts {
            let element = RevisedAustriaDistrict(
                id: UUID(),
                district: item.district,
                cases: item.cases.str,
                population: item.population.str
            )
            districts.append(element)
        }

        // Provinces:
        for item in origData.provinces {
            let element = RevisedAustriaProvince(
                id: UUID(),
                province: item.province,
                cases: item.cases.str,
                recovered: item.recovered.str,
                deaths: item.deaths.str
            )
            provinces.append(element)
        }

        return RevisedAustriaDataList(
            provinces: provinces,
            districts: districts,
            percentageBySex: origData.percentageBySex,
            casesByAge: origData.casesByAge,
            deathsByAge: origData.deathsByAge
        )
    }
}

// ----------------------------------------------------------------------------------------------
// TODO: Put extension into global resource folder.

extension Int {
    var str: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesSignificantDigits = false
        formatter.minimumIntegerDigits = 0
        formatter.groupingSeparator = ","
        let formattedString = formatter.string(for: self)
        return formattedString!
    }
}
