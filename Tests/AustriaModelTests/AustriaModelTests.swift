import XCTest
@testable import AustriaModel

@available(iOS 15.0.0, *)
final class AustriaModelTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(AustriaModel.text, "Hello, World!")
    }
    
    func textAustria() throws {
        let austria = AustriaModel.shared
        XCTAssertNotNil(austria)
    }
}
